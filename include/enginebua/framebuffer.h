#ifndef EB_FRAMEBUFFER
#define EB_FRAMEBUFFER 1

#include "types.h"

namespace EB {


	class Framebuffer {
	public:
		Framebuffer();
		~Framebuffer();
		
		///Functions
		void Init(const unsigned int width, const unsigned int height);
		
		///Framebuffer id
		unsigned int getID();
		///Color texture id
		unsigned int getColorID();
		///Depth texture id
		unsigned int getDepthID();
		///Shadow framebuffer
		unsigned int getCubemapID();

	private:
		struct FrameData;
		FrameData* framedata;
	};


}

#endif // !EB_FRAMEBUFFER

