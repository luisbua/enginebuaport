// ----------------------------------------------------------------------------
// Copyright (C) 2014 Jose L. Hidalgo 
// Scoped_ptr, original code from boost.
// ----------------------------------------------------------------------------
//  scoped_ptr mimics a built-in pointer except that it guarantees deletion
//  of the object pointed to, either on destruction of the scoped_ptr or via
//  an explicit reset(). scoped_ptr is a simple solution for simple needs;
// ----------------------------------------------------------------------------

#ifndef INCLUDE_SCOPED_PTR_H_
#define INCLUDE_SCOPED_PTR_H_

#include <cassert>

  template<class T> 
  class scoped_ptr { 
  public:

	  scoped_ptr() : ptr_(nullptr){}
	  ~scoped_ptr() {
		  reset();
	  }
	  T* alloc() {
		  reset();
		  ptr_ = new T();
		  return ptr_;
	  }
	  T* get() const {
		  return ptr_;
	  }
	  T* operator->() {
		  assert(ptr_ != 0);
		  return ptr_;
	  }
	  T* operator*() {
		  assert(ptr_ != 0);
		  return ptr_;
	  }
	  void reset() {
		  delete ptr_;
		  ptr_ = nullptr;
	  }
	  

  private:
	  T* ptr_;
	  explicit scoped_ptr(T* p) : px_(p) {
	  }

	  explicit scoped_ptr(scoped_ptr const&);
	  scoped_ptr& operator=(scoped_ptr const&);
  };

#endif  // INCLUDE_SCOPED_PTR_H_
