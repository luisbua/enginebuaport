#ifndef EB_LUAHANDLER
#define EB_LUAHANDLER 1

#include "lua.hpp"
//#include "gamestate.h"
//#include "shaders.h"
//#include "texture.h"



namespace EB {
	class LuaHandler {
		public:

			///Funtions
			void init();
			void execute_file(char* file_lua);

			lua_State *L;
		private:

	};
}
#endif // !EB_LUAHANDLER