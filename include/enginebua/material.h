#ifndef EB_MATERIAL 
#define EB_MATERIAL 1

#include "types.h"
#include "camera.h"
#include "shader.h"
#include "texture.h"

namespace EB {

	class Material {
	public:
		Material();
		~Material();

		///Functions
		void load(materialType mt);
		void compile();
		void use(glm::mat4 model, Camera *cam, eb_bool isHighLight);
		
		
		///Variables
		eb_uint32 vertexShader;
		eb_uint32 fragmentShader;
		eb_uint32 shaderProgram;

		eb_int32 texId;
		eb_int32 texId2;

		bool tex_ON;
		Texture texture, texture2;
		bool Is_Light;
		glm::vec3 nodePosition;
		glm::mat4 lightSpace;
		
		Shader shader;
		Shader shadowShader;
		Shader highlight;


	private:
		
	};
}
#endif // !EB_MATERIAL 1

