#ifndef EB_NODE
#define EB_NODE 1

#include "types.h"
#include "scoped_array.h"
#include "scoped_ptr.h"
#include "geometry.h"
#include "material.h"
#include "camera.h"
#include "component.h"
#include "light.h"
#include "framebuffer.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\matrix_decompose.hpp>
#include <vector>
#include <memory>

namespace EB {
	
	class Node {
	public:
		Node();
		~Node();

		
		///Functions
		Node* createRoot();
		Node* addChild();
		void removeChild(Node **n);
		void setTextureId(eb_uint32 id);
		void execute(Camera *cam);
			
		///to load prefab shapes
		Node* createNode(Node* node, glm::vec3 pos,Node* father, 
			geometryShape geoShape = kShape_Empty, materialType matType = kMaterial_Empty,
			std::string texture = "../../data/images/default.jpg",eb_float size = 1.0f);
		///to load obj
		Node* createNode(Node* node, glm::vec3 pos, Node* father,
			std::string file, materialType matType = kMaterial_Empty, 
			std::string texture = "../../data/images/default.jpg",eb_float size = 1.0f);
		
		///translate
		void setPosition(glm::vec3 position);
		void setRotation(glm::vec3 rotation, eb_float angle);
		void setScale(glm::vec3 scale);

		///build transform
		glm::mat4 transformCalculator();
		glm::mat4 transformCalculator2();

		///getters
		glm::vec3 getWorldPosition();

		//////////COMPONENTS/////////////
		////////////////////////////////
		//Geometries
		std::vector<ComponentGeometry> compGeo;
		struct GeoParams {
			geometryShape geoshape;
			eb_int32 size;
		};
		///overload method
		void addCompGeometry(geometryShape shape);
		void addCompGeometry(std::string file);
		
		
		///Materials
		std::vector<ComponentMaterial> compMat;
		struct MatParams {
			materialType mattype;

		};

		void addCompMaterial(materialType matType, 
			std::string texture);
				
		///Lights
		Light lightNode;
		eb_bool Is_Light;
		void setLight();
		glm::mat4 lightSpaceMatrix;
		
		///Camera
		Camera camNode;
		void setCamera();

		///HighLighy
		Geometry geoHigh;
		eb_bool isHighLight;
		void setHighLight();
		void executeHighLight(Camera *cam);


		//////////////////////////////////////////
		/////////////////////////////////////////
			
		///Variables
		int id;
		Node* father;
		Geometry geo;
		Material mat;
		
		glm::mat4 model;
		glm::mat4 transform, transform2;
		glm::vec3 pos,rot,sca,pos2,sca2;

	private:
		std::vector<std::unique_ptr<Node>> children_;
		eb_float ang;
		
	};
}

#endif // !EB_NODE

