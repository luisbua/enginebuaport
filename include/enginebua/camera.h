#ifndef EB_CAMERA
#define EB_CAMERA 1

#include "types.h"
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace EB {

	class Camera {
	public:
		Camera();
		~Camera();

		///Functions
		void Init(eb_float width, eb_float height);
		void Update(int program,
			glm::mat4 model,
			unsigned int texture_id);
		void SetAspectRatio(eb_float w, eb_float h);
		void SetPosition(glm::vec3 pos);
		void SetTarget(glm::vec3 tar);
		void SetNearFarPlane(eb_float near_plane,eb_float far_plane);
		glm::mat4 GetViewMatrix();
		void MoveCamera();
		

		///Camera parameters
		glm::vec3 camPos;
		glm::vec3 camTarget;
		glm::vec3 camDirection;
		glm::vec3 camUp;
		glm::vec3 camRight;
		glm::vec3 worldUp;

		
		eb_float aspectRatio;
		eb_float farPlane;
		eb_float nearPlane;
		eb_float camVelocity;
				
		///Matrix
		glm::mat4 view;
		glm::mat4 projection;
		glm::mat4 lightSpaceMatrix;
		
		
	};
}

#endif // !EB_CAMERA
