#ifndef EB_ENGINE
#define EB_ENGINE 1

#include "chrono.h"
#include "window.h"
#include "geometry.h"
#include "shader.h"
#include "material.h"
#include "camera.h"
#include "node.h"
#include "command.h"
#include "component.h"
#include "luahandler.h"

#include "bx\thread.h"
#include <vector>
#include "jsoncons\json.hpp"


namespace EB{

	class Engine
	{
	private:
		
		///Functions
		Engine();
		~Engine();
		
		//Variables
		static Engine *instance_;
	
	public:
		
		///Functions
		void Init(eb_int32 width = 800, eb_int32 height = 600);
		
		///Variables
		Window* window;
		Chrono* chrono;
		Camera* camera;
		Node root;
		std::vector<Node*> nodes;
		std::vector<Node*> lights;
		std::vector<Command> command_list;
		Command command_aux;
		bx::Semaphore s1, s2;
		LuaHandler luaHandler;
		eb_float deltaTime;
		eb_float lastFrame;
		eb_float currentFrame;
		eb_int32 postprocess;
		
		///Variables Framebuffer
		eb_uint32 fbo; //frame buffer object
		eb_uint32 sfb[10]; //shadow buffer
		eb_int32 depht[10]; //depht buffer
		glm::mat4 lightMatrix[10];
		eb_uint32 width, height; //screen size
		
		///Variables Json
		jsoncons::json rootJson;
		std::vector<jsoncons::json> data;
		jsoncons::json jsonstart;
		jsoncons::json jsonend;
		eb_uint32 numUpdates,numDraw;
		double startUpdate, endUpdate;
		double startDraw, endDraw;
		
		///Functions Json
		void StartChrono(const char *name, int frame, float time);
		void EndChrono(const char *name, int frame, float time);

		///Singleton Instance
		static Engine* Instance() {
			if (!instance_) {
				instance_ = new Engine;
			}
			return instance_;
		}

	};

}

#endif // !EB_ENGINE

