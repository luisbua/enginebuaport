#ifndef EB_COMPONENT
#define EB_COMPONENT 1


namespace EB {

	class Node;
	class  Component
	{
	public:
		 Component();
		~ Component();
		Node *node;
	private:

	};

	class ComponentGeometry: public Component {
	public:
		ComponentGeometry();
		~ComponentGeometry();
		struct GeometryParams {
			float a;
			float b;

			static GeometryParams init(float a) { return { a*5.0f, a*6.0f }; }
		}gp;

	private:

	};

	class ComponentMaterial : public Component {
	public:
		ComponentMaterial();
		~ComponentMaterial();
	private:

	};

}

#endif // !EB_COMPONENT


