#ifndef EB_TYPES
#define EB_TYPES 1

#include <stdint.h>
#include <iostream>

namespace EB {
#ifdef _WIN32
	typedef int8_t  eb_int8;
	typedef int16_t eb_int16;
	typedef int32_t eb_int32;
	typedef int64_t eb_int64;

	typedef uint8_t  eb_uint8;
	typedef uint16_t eb_uint16;
	typedef uint32_t eb_uint32;
	typedef uint32_t eb_uint64;

	typedef char   eb_char;
	typedef unsigned char eb_uintchar;
	typedef float  eb_float;
	typedef double eb_double;
	typedef bool   eb_bool;

#endif // _WIN32

	enum geometryShape {
		kShape_Empty,
		kShape_Cube,
		kShape_Triangle,
		kShape_Quad,
		kShape_Obj,
		kShape_RenderQuad
	};


	enum commandType {
		kCommand_Empty,
		kCommand_Draw,
		kCommand_HighLight
	 };

	enum componentType {
		kComponent_Empty = 200,
		kComponent_Geometry = 201,
		kComponent_Material = 202,
		kComponent_Light = 203
	};

	enum framebufferType {
		kFramebuffer_Normal,
		kFramebuffer_TextureColor,
		kFramebuffer_Rbo
	};
	
	enum materialType {
		kMaterial_Empty = 100,
		kMaterial_Standard = 101,
		kMaterial_Shadow = 102,
		kMaterial_Postprocess = 103,
		kMaterial_HigLight = 104
	};

	
	struct Vec2 {
		eb_float x, y;
	};
	struct Vec3 {
		eb_float x, y, z;
	};
	struct Vec4 {
		eb_float x, y, z, w;
	};
}
#endif