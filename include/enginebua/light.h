#ifndef EB_LIGHT
#define EB_LIGHT 1

#include "types.h"
#include "glm\glm.hpp"
#include "framebuffer.h"
#include "camera.h"


namespace EB {

	class Light {
	public:
		Light();
		~Light();

		///Functions
		void Init();
		glm::mat4 SetLightSpace();

		///Variables
		eb_int32 id;
		glm::vec3 pos,worldPos;

		Framebuffer fbLight;
		Camera camLight;
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		glm::vec3 direction;


	private:

	};


}
#endif // !EB_LIGHT

