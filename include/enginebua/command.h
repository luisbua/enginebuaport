#ifndef EB_COMMAND
#define EB_COMMAND 1

#include "types.h"
#include "node.h"
#include "camera.h"

namespace EB {

	class Command {
	public:
		Command();
		~Command();

		commandType comand_type;
		Node *node;
		Camera *camera;

		void execute();
		void executeShadows(Camera cam);
		void Create(commandType ct, Node *n, Camera *cam);

	};
}

#endif