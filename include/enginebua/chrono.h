#ifndef EB_CHRONO
#define EB_CHRONO 1

namespace EB {

	class Chrono {
	public:
		Chrono();
		~Chrono();

		void Start();
		double Stop();
		void ShowTime();
	private:
		double init_;
		double end_;

	};
}

#endif // !EB_CHRONO
