#ifndef EB_TEXTURE__
#define EB_TEXTURE__ 1


#include "types.h"
#include "scoped_ptr.h"

namespace EB {
	class Texture {
	public:

		///Functions
		Texture();
		~Texture();
		void load(const char* str);
		void compile();
		eb_uint32 getTexID();

		///Variables
		eb_uint32 texture;

	private:
		struct Data;
		scoped_ptr<Data> ptr_;
		eb_int32 width, height, nrChannels;
		eb_uintchar *dataTexture;
	};
}
#endif // EB_TEXTURE__