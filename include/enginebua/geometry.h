#ifndef EB_GEOMETRY 
#define EB_GEOMETRY 1

#include "types.h"
#include "scoped_array.h"
#include "scoped_ptr.h"
#include <vector>



namespace EB {


	class Geometry {
	public:
		Geometry();
		~Geometry();

		///Functions

		///Load a prefab Shape
		void load(geometryShape gsh);
		///Load a obj object
		void load(std::string file);
		void compile();
		void draw();
		
		///Variables
		eb_uint32 VBO, VAO, EBO;
		eb_float size;
				
	private:
		
		struct VertexData {
					
			Vec3 vertex;
			Vec3 normal;
			Vec2 texcoord;
		};

		std::vector<eb_uint32> indexData;
		std::vector<VertexData> vertexData;
		
		VertexData vertex_aux;
		
	};


}
#endif // !EB_GEOMETRY 
