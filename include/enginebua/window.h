#ifndef EB_WINDOW
#define EB_WINDOW 1

#include "types.h"


namespace EB {

	class Window {
	private:
		struct WinData;
		eb_bool show_another_window = false;
		eb_bool frame_buffer_ON = false;

	public:
		Window();
		~Window();

		///Functions
		int Init(int width = 640, int height = 480);
		void Clear();
		void Swap();
		void PollEvents();
		void ImGuiInterface();
		bool CloseWindow();
		void Terminate();
		int CatchKeyboard();

		void InitRender(const int id);
		void EndRender();
		void initFrameBuffer();


		///Variables
		eb_int16 width_screen, height_screen;
		WinData *windata;
		eb_float closeWindow;
				
	};

}


#endif // !EB_WINDOW 
