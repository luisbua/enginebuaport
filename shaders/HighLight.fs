#version 330 core
out vec4 FragColor;

in vec2 TexCoords;
in vec3 Normal;
in vec3 Fragpos;

uniform sampler2D textureMap;
uniform vec3 viewPos;


void main()
{
	vec3 normal_Normalized = normalize(Normal); 
	vec3 Frag_Normalized = normalize(Fragpos);
	float a  = pow(max(dot(normal_Normalized, -Frag_Normalized), 0.0), 2.0);

	FragColor = vec4(vec3(1.0 - texture(textureMap, TexCoords)),a);
}