#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec4 FragPosLightSpace[10];
    
} fs_in;


uniform sampler2D textureMap;
uniform sampler2D shadowMap0;
uniform sampler2D shadowMap1;
uniform sampler2D shadowMap2;
uniform sampler2D shadowMap3;




uniform vec3 lightPos[10];
uniform vec3 viewPos;
uniform int numLigths;

bool blinn;
vec3 lighting;
float shadow0 ,shadow1,shadow2,shadow3;
vec3 ambient;

float ShadowCalculation(vec4 fragPosLightSpace, sampler2D texShadow)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(texShadow, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
	float bias = 0.005;
    // check whether current frag pos is in shadow
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}

void main()
{           
   	blinn = false;
    vec3 color = texture(textureMap, fs_in.TexCoords).rgb;
	vec3 normal = normalize(fs_in.Normal);
	vec3 lightColor = vec3(0.1);
	

    for (int i = 0; i < numLigths; i++)
    {
      
      // ambient
      ambient = 0.5 * color;
      
      // diffuse
      vec3 lightDir = normalize(lightPos[i] - fs_in.FragPos);
      float diff = max(dot(lightDir, normal), 0.0);
      //vec3 diffuse = diff * color;
      vec3 diffuse = diff * lightColor;
      
      // specular
      vec3 viewDir = normalize(viewPos - fs_in.FragPos);
      vec3 reflectDir = reflect(-lightDir, normal);
      float spec = 0.0;
      
      if(blinn)
      {
          vec3 halfwayDir = normalize(lightDir + viewDir);  
          spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
      }
      else
      {
          vec3 reflectDir = reflect(-lightDir, normal);
          spec = pow(max(dot(viewDir, reflectDir), 0.0), 64.0);
      }
      vec3 specular = spec * lightColor; // assuming bright white light color
      
     
      lighting = (((ambient + diffuse + specular))*(1.0/numLigths));
      
    }

    if(numLigths == 1){
    	shadow0 = ShadowCalculation(fs_in.FragPosLightSpace[0],shadowMap0);
    }else if (numLigths == 2){
		shadow0 = ShadowCalculation(fs_in.FragPosLightSpace[0],shadowMap0);
   		shadow1 = ShadowCalculation(fs_in.FragPosLightSpace[1],shadowMap1);
    }else if (numLigths == 3){
    	shadow0 = ShadowCalculation(fs_in.FragPosLightSpace[0],shadowMap0);
    	shadow1 = ShadowCalculation(fs_in.FragPosLightSpace[1],shadowMap1);
    	shadow2 = ShadowCalculation(fs_in.FragPosLightSpace[2],shadowMap2);
    }else if (numLigths == 4){
	    shadow0 = ShadowCalculation(fs_in.FragPosLightSpace[0],shadowMap0);
	    shadow1 = ShadowCalculation(fs_in.FragPosLightSpace[1],shadowMap1);
	    shadow2 = ShadowCalculation(fs_in.FragPosLightSpace[2],shadowMap2);
	    shadow3 = ShadowCalculation(fs_in.FragPosLightSpace[3],shadowMap3);	
    }

 	
    if (numLigths <= 0)
    {
    	FragColor = vec4(color, 1.0) * 0.7;
    }
    else
    {
    	FragColor = (((1.0 - shadow0) + (1.0 - shadow1)+(1.0 - shadow2) + (1.0 - shadow3))) * vec4(lighting, 1.0) *0.5;
    } 
}