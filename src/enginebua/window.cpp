#include <iostream>
#include "window.h"
#include "heads.h"
#include "node.h"
#include "camera.h"
#include "framebuffer.h"
#include "glad/glad.h"
#include "GLFW/include/GLFW/glfw3.h"

#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

#include <string>


namespace EB {
	
	struct Window::WinData {
		GLFWwindow* window;
		eb_int16 width_, height_;
		Node* N;
		Camera Cam;
		Framebuffer fbo;
	};

	

	void framebuffer_size_callback(GLFWwindow* window, int width, int height)
	{
		// make sure the viewport matches the new window dimensions; note that width and 
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, width, height);
	}

	

	Window::Window(){
		windata = new WinData();
	}
	Window::~Window(){
		delete windata;
	}

	int EB::Window::Init(int width,int height)
	{
		EB::Engine *engine = EB::Engine::Instance();
		
		if (!glfwInit())
			return -1;

		height_screen = height;
		width_screen = width;

		windata->width_ = width;
		windata->height_ = height;

		engine->width = width;
		engine->height = height;

		closeWindow = false;
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		windata->window = glfwCreateWindow(width,height, "Engine Bua", NULL, NULL);
		glfwSetInputMode(windata->window, GLFW_CURSOR_NORMAL, true);
		
		if (!windata->window)
		{
			glfwTerminate();
			return -1;
		}
		glfwMakeContextCurrent(windata->window);
		glfwSetFramebufferSizeCallback(windata->window, framebuffer_size_callback);

		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			std::cout << "Failed to initialize GLAD" << std::endl;
			return -1;
		}
		
		glEnable(GL_DEPTH_TEST);
		//glEnable(GL_BLEND);
		//glDisable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		//wireframe mode
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		//Init ImGui
		ImGui::CreateContext();
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		ImGui_ImplGlfwGL3_Init(windata->window, true);
		
		// Setup ImGui style
		ImGui::StyleColorsDark();


		return 0;
	}



	void Window::PollEvents()
	{
		glfwPollEvents();
	}

	void Window::ImGuiInterface()
	{
		EB::Engine *engine = EB::Engine::Instance();
		static float f = 0.0f;
		static int counter = 0;

		ImGui_ImplGlfwGL3_NewFrame();

		//ImGui::Text("Hello, world!");
		//ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f    


		if (ImGui::Button("FPS")) {                          // Buttons return true when clicked (NB: most widgets return true when edited/activated)
			show_another_window = true;
				
		}

		static int e = 0;
		ImGui::Text("Select Postprocess");
		ImGui::RadioButton("Without", &e, 0); ImGui::SameLine();
		ImGui::RadioButton("Blur", &e, 1); ImGui::SameLine();
		ImGui::RadioButton("Negative", &e, 2); ImGui::SameLine();
		ImGui::RadioButton("Edge", &e, 3);

		engine->postprocess = e;
		
		ImGui::NewLine();
		ImGui::Text("To move camera");
		ImGui::Text("W: Forward"); 
		ImGui::Text("S: Backward"); 
		ImGui::Text("A: Rotate Left"); 
		ImGui::Text("D: Rotate Right");
		ImGui::Text("E: Up"); 
		ImGui::Text("Q: Down");
		

		if (show_another_window)
		{
			//ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::SetNextWindowPos(ImVec2(0, 0));
			ImGui::Begin("BCKGND", NULL, ImGui::GetIO().DisplaySize, 0.0f, 
				ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | 
				ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | 
				ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoCollapse | 
				ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoInputs |
				ImGuiWindowFlags_NoFocusOnAppearing | 
				ImGuiWindowFlags_NoBringToFrontOnFocus);
				std::string fps = std::to_string(ImGui::GetIO().Framerate);
				fps.replace(fps.begin() + 4, fps.begin(), " FPS");
				ImGui::GetWindowDrawList()->AddText(ImVec2(900, 10), 
					ImColor(1.0f, 1.0f, 1.0f, 1.0f),fps.c_str());
				ImGui::End();
		}


		ImGui::Render();
		ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
	}

	bool Window::CloseWindow()
	{
		return glfwWindowShouldClose(windata->window);
	}

	void Window::Terminate()
	{
		glfwTerminate();
	}


	void Window::Swap()
	{
		ImGuiInterface();
		glfwSwapBuffers(windata->window);
	}


	void Window::InitRender(const int id) {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void Window::EndRender() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(0.0f, 1.0f, 0.0f, 1.0f); 
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	void Window::Clear()
	{
		if (frame_buffer_ON) {
			windata->N->execute(&windata->Cam);
		}
		
	}

	int Window::CatchKeyboard()
	{
		
		
		if (glfwGetKey(windata->window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			closeWindow = true;
			return 27;
		}
		if (glfwGetKey(windata->window, GLFW_KEY_W) == GLFW_PRESS)
		{
			return 87; //Ascii W
		}
		if (glfwGetKey(windata->window, GLFW_KEY_S) == GLFW_PRESS)
		{
			return 83; //Ascii S
		}
		if (glfwGetKey(windata->window, GLFW_KEY_A) == GLFW_PRESS)
		{
			return 65; //Ascii A
		}
		if (glfwGetKey(windata->window, GLFW_KEY_D) == GLFW_PRESS)
		{
			return 68; //Ascii D
		}
		if (glfwGetKey(windata->window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			return 81; //Ascii Q
		}
		if (glfwGetKey(windata->window, GLFW_KEY_E) == GLFW_PRESS)
		{
			return 69; //Ascii E
		}
	}

	void Window::initFrameBuffer() {
		EB::Engine *engine = EB::Engine::Instance();
		windata->fbo.Init(windata->width_, windata->height_);
		engine->fbo = windata->fbo.getID();
		
		windata->N = windata->N->createRoot();
		windata->N->setPosition(glm::vec3(0.0f,0.0f,-10.0f));
				windata->N->geo.load(kShape_RenderQuad);
		
		
		windata->N->mat.load(kMaterial_Postprocess);
		
		//color
		windata->N->mat.texId = windata->fbo.getColorID();
		
		//depht //uncoment to view depht map
		//windata->N->mat.texId = engine->depht[0];
		//windata->N->mat.texId = engine->depht[1];
		//windata->N->mat.texId = engine->depht[2];
		//windata->N->mat.texId = engine->depht[3];
		
		windata->N->geo.compile();
		windata->N->mat.compile();
		windata->N->mat.tex_ON = true;
		windata->Cam.Init(1440, 900);
		windata->Cam.SetPosition(glm::vec3(0.0f, 20.0f, 20.0f));
		windata->Cam.SetTarget(glm::vec3(0.0f, 0.0f, 0.0f));

		
		frame_buffer_ON = true;
	}
}