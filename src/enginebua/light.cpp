#include "light.h"
#include "heads.h"



namespace EB {


	EB::Light::Light()
	{
		id = 0;
	}

	EB::Light::~Light()
	{
	}

	void Light::Init()
	{
		EB::Engine *engine = EB::Engine::Instance();
		
		camLight.Init(engine->width,engine->height);
		camLight.SetPosition(pos);
		direction = camLight.camTarget - camLight.camPos;
		
		///Create framebuffer
		fbLight.Init(engine->width, engine->height);
		///Set values to Engine
		engine->sfb[id] = fbLight.getID();
		engine->depht[id] = fbLight.getDepthID();
		engine->lightMatrix[id] = SetLightSpace();

	}

	glm::mat4 Light::SetLightSpace()
	{
		lightView = camLight.GetViewMatrix();
		lightProjection = glm::perspective(glm::radians(45.0f), camLight.aspectRatio, camLight.nearPlane, camLight.farPlane);
		lightSpaceMatrix = lightProjection * lightView;

		return lightSpaceMatrix;
	}
}