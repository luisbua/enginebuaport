#include "node.h"

#include "heads.h"
#include <stdarg.h>

namespace EB {

	
	class Engine;
	Node::Node() {

		pos = glm::vec3(0.0f,0.0f,0.0f);
		ang = 0;
		rot = glm::vec3(1.0f, 1.0f, 1.0f);
		sca = glm::vec3(1.0f, 1.0f, 1.0f);
		Is_Light = false;
		isHighLight = false;
	}

	Node::~Node() {
	}

	Node* Node::addChild() {
		//Should attach father
		std::unique_ptr<Node> n(new Node());
		n->father = this;
		Node* p = n.get();
		children_.push_back(std::move(n));
		return p;
	}
	
	Node* Node::createRoot() {
		Node* n = new Node();
		n->father = nullptr;
		return n;
	}

	void Node::removeChild(Node **n) {
		for (eb_uint32 i = 0; i < children_.size(); i++) {
			if (children_[i].get() == *n) {
				children_.erase(children_.begin() + i);
				printf("Delete: %u\n", *n);
				*n = nullptr;
			}
		}
	}

	void Node::setTextureId(eb_uint32 id)
	{
		mat.texId = id;
	}

	void Node::setLight()
	{
		EB::Engine *engine = EB::Engine::Instance();

		Is_Light = true;
		engine->lights.push_back(engine->nodes.back());
		engine->nodes.pop_back();
		lightNode.id = engine->lights.size() - 1;
		lightNode.pos = pos;
		lightNode.worldPos = getWorldPosition();
		lightNode.Init();
		mat.lightSpace = lightSpaceMatrix;
	}
	
	void Node::setCamera()
	{
		EB::Engine *engine = EB::Engine::Instance();
		camNode.Init(engine->window->width_screen, engine->window->height_screen);
		camNode.SetPosition(pos);
		
	}

	void Node::setHighLight()
	{
		isHighLight = true;
		mat.load(kMaterial_HigLight);
		mat.texture2.load("../../data/images/hexa.jpg");
		mat.texId2 = mat.texture2.getTexID();
		geoHigh.load(kShape_Cube);
		geoHigh.compile();
	}


	Node * Node::createNode(Node * node, glm::vec3 pos, Node * father,
		 std::string file, materialType matType,  std::string texture,eb_float size)
	{
		EB::Engine *engine = EB::Engine::Instance();
		node = father->addChild();
		node->pos = pos;
		node->geo.size = size;
		node->model = glm::translate(node->model, pos);
		node->addCompGeometry(file);
		node->addCompMaterial(matType,texture);
		node->id = engine->nodes.size();
		engine->nodes.push_back(node);
	
		
		return node;
	}

	Node* Node::createNode(Node* node, glm::vec3 pos, Node* father,
		 geometryShape geoShape, materialType matType, std::string texture,eb_float size)
	{
		EB::Engine *engine = EB::Engine::Instance();
		node = father->addChild();
		node->pos = pos;
		node->geo.size = size;
		node->model = glm::translate(node->model, pos);
		node->addCompGeometry(geoShape);
		node->addCompMaterial(matType,texture);
		node->id = engine->nodes.size();
		engine->nodes.push_back(node);
			
		return node;
	}

	void Node::setPosition(glm::vec3 position)
	{
		pos = position;
	}

	void Node::setRotation(glm::vec3 rotation, eb_float angle)
	{
		rot = rotation;
		ang = angle;
	}

	void Node::setScale(glm::vec3 scale)
	{
		sca = scale;
		
	}

	glm::mat4 Node::transformCalculator()
	{
		glm::mat4 t;
		if (father != NULL) {
			t *= father->transform;
		}

		t = glm::rotate(t, glm::radians(ang), rot);
		t = glm::translate(t, pos);
		t = glm::scale(t, sca);

		return t;
	}

	glm::vec3 Node::getWorldPosition()
	{
		transformCalculator();
		return glm::vec3(transform[3]);
	}
	

	void Node::addCompGeometry(geometryShape shape) {
		
		
		geo.load(shape);
		geo.compile();

	}

	void Node::addCompGeometry(std::string file)
	{
		geo.load(file);
		geo.compile();
	}

	void Node::addCompMaterial(materialType matType, std::string tex) {
				
		EB::Engine *engine = EB::Engine::Instance();
		mat.load(matType); 
		mat.texture.load(tex.c_str());
		mat.texId = mat.texture.getTexID();
		mat.tex_ON = true;
		mat.compile();
	}

	void Node::execute(Camera *cam)
	{
		EB::Engine *engine = EB::Engine::Instance();
		
		transform = transformCalculator();


		if (Is_Light) {
			
			glm::vec3 auxPos;
			auxPos = getWorldPosition();
			lightNode.camLight.SetPosition(auxPos);
			engine->lightMatrix[lightNode.id] = lightNode.SetLightSpace();
		}
		mat.use(transform,cam,false);
		geo.draw();
		
	}

	glm::mat4 Node::transformCalculator2()
	{
		glm::mat4 t;
		if (father != NULL) {
			t *= father->transform;
		}

		t = glm::rotate(t, glm::radians(ang), rot);
		t = glm::translate(t, pos2);
		t = glm::scale(t, sca2);

		return t;
	}

	void Node::executeHighLight(Camera *cam)
	{
		EB::Engine *engine = EB::Engine::Instance();
		glm::vec3 des = { 2.0,2.0,2.0 };
		glm::vec3 scale2 = { 1.1,1.1,1.1 };
		pos2 = pos;
		sca2 = scale2;
		transform2 = transformCalculator2();
		mat.use(transform2, cam, isHighLight);
		geoHigh.draw();
	}
}