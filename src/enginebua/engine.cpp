
#include "engine.h"

namespace EB{

	Engine::Engine()
	{

	}

	Engine::~Engine()
	{
		delete window;
		delete chrono;
		delete camera;
	}

	void Engine::Init(eb_int32 width, eb_int32 height) {
		window = new Window();
		chrono = new Chrono();
		camera = new Camera();
	
		root.createRoot();
				
		window->Init(width,height);
		camera->Init(width, height);
				
		numUpdates = 0;
		numDraw = 0;
		startUpdate = 0;
		endUpdate = 0;
		startDraw = 0;
		endDraw = 0;
		deltaTime = 0;
		lastFrame = 0;
		postprocess = 0;

	}

	void Engine::StartChrono(const char *name, int frame, float time) {
		char num[256];
		itoa(frame, num, 10);
		jsonstart["name"] = num;
		jsonstart["ph"] = "B";
		jsonstart["pid"] = 1;
		jsonstart["tid"] = name;
		jsonstart["ts"] = time;
		data.push_back(jsonstart);
	}

	void Engine::EndChrono(const char *name, int frame, float time) {
		char num[256];
		itoa(frame, num, 10);
		jsonend["name"] = num;
		jsonend["ph"] = "E";
		jsonend["pid"] = 1;
		jsonend["tid"] = name;
		jsonend["ts"] = time;
		data.push_back(jsonend);
	}
	
	Engine* Engine::instance_;
}