#include "luahandler.h"
#include "heads.h"
#include <iostream>

namespace EB {

	Texture *tx_;
	int sun_cont = 0;
	int earth_cont = 0;
	int moon_cont = 0;
	Node *n;

	EB::Engine *engine = EB::Engine::Instance();

	int addNode(lua_State *L)
	{
		//Create Node with parameters
		Node* father;
		EB::geometryShape geo;
		EB::materialType mat;
		//eb_float size = 1.0f;
		eb_bool is_obj = false;
		
		///Push data from lua stack
		int id_father = lua_tonumber(L, 1);
		float  posX = lua_tonumber(L, 2);
		float posY = lua_tonumber(L, 3);
		float posZ = lua_tonumber(L, 4);
		bool is_light = lua_toboolean(L, 5);
		float size = lua_tonumber(L, 6);
		const char* geometry = lua_tostring(L, 7);
		const char* material = lua_tostring(L, 8);
		const char* tex = lua_tostring(L, 9);
		
		if (id_father != -1)
			father = engine->nodes[id_father];
		else
			father = &engine->root;

		glm::vec3 pos = { posX,posY,posZ };
	
		///select Geometry
		if (geometry != NULL) {

			if (strcmp(geometry, "Quad") == 0) 
			{
				geo = EB::kShape_Quad;
			}
			else if (strcmp(geometry, "Cube") == 0)
				 {
					geo = EB::kShape_Cube;
				 }
			else is_obj = true;
		}
		else {
			geo = EB::kShape_Cube;
		}

		///Select Material
		if (material != NULL) {

			if (strcmp(material, "Standard") == 0)
			{
				mat = EB::kMaterial_Standard;
			}
		}else {
				mat = EB::kMaterial_Empty;
		}

		if (tex == NULL) {
			tex = "../../data/images/default.jpg";
		}

		if (size == NULL) {
			size = 1.0;
		}

		if (is_obj) {
			n = n->createNode(n, pos, father, geometry, mat, tex, size);
		}
		else {
			n = n->createNode(n, pos, father, geo, mat, tex, size);
		}
		if (is_light) {
			n->setLight();
		}
		
		lua_pushinteger(L, n->id);
		
		return 1;
	}
	void LuaHandler::init()
	{
		L = luaL_newstate();
		luaL_openlibs(L);
		lua_register(L, "addNode", addNode);
	}

	void LuaHandler::execute_file(char* file_lua)
	{
		if (luaL_dofile(L, file_lua)) {
			std::cout << "ERROR IN LUA " << lua_tostring(L, -1) << std::endl;
			lua_pop(L, 1);
		}
	}


}