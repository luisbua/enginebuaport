#include "texture.h"
#include "glad\glad.h"


#define STB_IMAGE_IMPLEMENTATION
#include "stb\stb_image.h"


struct EB::Texture::Data {
	GLuint tex;
	eb_int32 width;
	eb_int32 height;
	eb_int32 nrChannels;
	//unsigned char *image;
};

EB::Texture::Texture() {
	ptr_.alloc();
	ptr_->tex = 0;
	ptr_->width = 128;
	ptr_->height = 128;
}

EB::Texture::~Texture() {
	//glDeleteTextures(1, &ptr_->tex);
}

void EB::Texture::load(const char* str) {
	
	dataTexture = stbi_load(str,&width,&height,&nrChannels,0);
	compile();
}

void EB::Texture::compile() {
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	//flip texture to view correctly
	stbi_set_flip_vertically_on_load(true);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,
		GL_RGB, GL_UNSIGNED_BYTE, dataTexture);
	glGenerateMipmap(GL_TEXTURE_2D);

	stbi_image_free(dataTexture);
	printf("\n\n Texture ID: %d", texture);
	
}

EB::eb_uint32 EB::Texture::getTexID() {
	return texture;
}

