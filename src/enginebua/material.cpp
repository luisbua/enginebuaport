#include "material.h"
#include "heads.h"
#include "glad\glad.h"

namespace EB {

	EB::Material::Material()
	{
		texId = -1;
		tex_ON = false;
		Is_Light = false;
		
	}

	EB::Material::~Material()
	{

	}
	void Material::load(materialType mt)
	{
		switch (mt)
		{
		case EB::kMaterial_Empty:
			shader.Init("../../data/shaders/Simple.vs", "../../data/shaders/Simple.fs");
			
			break;
		case EB::kMaterial_Standard:
			shader.Init("../../data/shaders/Source.vs", "../../data/shaders/Source.fs");
			
			break;
		
		case EB::kMaterial_Postprocess:
			shader.Init("../../data/shaders/FrameScreen.vs", "../../data/shaders/FrameScreen.fs");
			
			break;

		case EB::kMaterial_HigLight:
			highlight.Init("../../data/shaders/HighLight.vs", "../../data/shaders/HighLight.fs");
			break;
		
		default:
			break;
		}
		
	}
	void Material::compile()
	{
		shader.use();
				
	}
	
	void Material::use(glm::mat4 transform, Camera *cam,eb_bool isHighLight)
	{
		EB::Engine *engine = EB::Engine::Instance();
		
		glm::mat4 model;
		model = model * transform;

		if (isHighLight) {
			glEnable(GL_BLEND);
			highlight.use();
			cam->Update(highlight.ID, model, texId2);
		}
		else {
			glDisable(GL_BLEND);
			shader.use();
			cam->Update(shader.ID,model,texId);
		}

	}
	
}
