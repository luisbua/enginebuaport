#include "framebuffer.h"
#include "glad\glad.h"
#include "glm\glm.hpp"

#include <iostream>



namespace EB {
	
	struct Framebuffer::FrameData {
		GLuint framebuffer;
		GLuint color;
		GLuint depth;
		GLuint rbo;
		GLuint cubemap;
	};

	Framebuffer::Framebuffer() {
		framedata = new Framebuffer::FrameData();
	}

	Framebuffer::~Framebuffer() {
		delete framedata;
		glDeleteFramebuffers(1, &framedata->framebuffer);
	}

	void Framebuffer::Init(const unsigned int width, const unsigned int height) {
		
		// color attachment texture
		glGenTextures(1, &framedata->color);
		glBindTexture(GL_TEXTURE_2D, framedata->color);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		// Depth attachment texture
		glGenTextures(1, &framedata->depth);
		glBindTexture(GL_TEXTURE_2D, framedata->depth);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		//------------------------ FrameBuffer
		glGenFramebuffers(1, &framedata->framebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, framedata->framebuffer);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framedata->color, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, framedata->depth, 0);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			std::cout << "Framebuffer is not complete!" << std::endl;

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		printf("\n\n\nColorBuffer  id: %d", framedata->framebuffer);
		printf("\nColorTexture id: %d", framedata->color);
		printf("\nDepthTexture id: %d", framedata->depth);
	}

	

	unsigned int Framebuffer::getID() {
		return framedata->framebuffer;
	}

	unsigned int Framebuffer::getColorID() {
		return framedata->color;
	}

	unsigned int Framebuffer::getDepthID() {
		return framedata->depth;
	}

	unsigned int Framebuffer::getCubemapID()
	{
		return framedata->cubemap;
	}

}

