#include "geometry.h"
#include "glad\glad.h"
#include "glm\glm.hpp"

#define TINYOBJLOADER_IMPLEMENTATION 
#include "tinyobj\tiny_obj_loader.h"

namespace EB {


	EB::Geometry::Geometry()
	{
		size = 1.0f;
	}

	EB::Geometry::~Geometry()
	{
	}

	///load prefab Shape
	void EB::Geometry::load(geometryShape gsh)
	{
		
		switch (gsh)
		{
		case EB::kShape_Empty:
			vertex_aux.vertex = { 0, 0, 0 };
			vertex_aux.normal = { 0,0,0 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);

			indexData.push_back(0);
			
			break;
		case EB::kShape_Cube:

			//Vertex
			//Up
			vertex_aux.vertex = { -size, size, -size };
			vertex_aux.normal = { 0,1,0 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			
			vertex_aux.vertex = { size, size, -size };
			vertex_aux.normal = { 0,1,0 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);
			
			vertex_aux.vertex = { size, size, size };
			vertex_aux.normal = { 0,1,0 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			
			vertex_aux.vertex = { -size, size, size };
			vertex_aux.normal = { 0,1,0 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);


			
			//rear quad
			//top
			vertex_aux.vertex = { size, size, -size };
			vertex_aux.normal = { 1,0,0 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { size, -size, -size };
			vertex_aux.normal = { 1,0,0 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { size, -size, size };
			vertex_aux.normal = { 1,0,0 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { size, size, size };
			vertex_aux.normal = { 1,0,0 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);

			//front quad
			//top
			vertex_aux.vertex = { size, size, size };
			vertex_aux.normal = { 0,0, 1};
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { size, -size, size };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { -size, -size, size };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { -size, size, size };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);



			//rear quad
			//top
			vertex_aux.vertex = { -size, size, -size };
			vertex_aux.normal = { -1,0,0 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { -size, -size, -size };
			vertex_aux.normal = { -1,0,0 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { -size, -size, size };
			vertex_aux.normal = { -1,0,0 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { -size, size, size };
			vertex_aux.normal = { -1,0,0 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);

			//front quad
			//top
			vertex_aux.vertex = { -size, -size, -size };
			vertex_aux.normal = { 0,-1,0};
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { -size, -size, size };
			vertex_aux.normal = { 0,-1,0 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { size, -size, size };
			vertex_aux.normal = { 0,-1,0 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { size, -size, -size };
			vertex_aux.normal = { 0,-1,0 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);



			//rear quad
			//top
			vertex_aux.vertex = { -size, size, -size };
			vertex_aux.normal = { 0,0,-1 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { size, size, -size };
			vertex_aux.normal = { 0,0,-1 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { size, -size, -size };
			vertex_aux.normal = { 0,0,-1 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { -size, -size, -size };
			vertex_aux.normal = { 0,0,-1 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
													  
			//Index
			//Front			  
			indexData.push_back(0);
			indexData.push_back(3);
			indexData.push_back(2);
			indexData.push_back(2);
			indexData.push_back(1);
			indexData.push_back(0);
			//Back	   	
			indexData.push_back(4);
			indexData.push_back(7);
			indexData.push_back(6);
			indexData.push_back(6);
			indexData.push_back(5);
			indexData.push_back(4);
			//Top	   	 
			indexData.push_back(8);
			indexData.push_back(11);
			indexData.push_back(9);
			indexData.push_back(11);
			indexData.push_back(10);
			indexData.push_back(9);
			//Bottom   	 
			indexData.push_back(12);
			indexData.push_back(13);
			indexData.push_back(14);
			indexData.push_back(15);
			indexData.push_back(12);
			indexData.push_back(14);
			//Right	   	 
			indexData.push_back(16);
			indexData.push_back(19);
			indexData.push_back(18);
			indexData.push_back(18);
			indexData.push_back(17);
			indexData.push_back(16);
			//Left	   	 
			indexData.push_back(20);
			indexData.push_back(21);
			indexData.push_back(22);
			indexData.push_back(22);
			indexData.push_back(23);
			indexData.push_back(20);
					   
			break;
		
		case EB::kShape_Quad:
			
			//top
			vertex_aux.vertex = { -size, size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { -size, -size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { size, -size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { size, size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);


			indexData.push_back(0);
			indexData.push_back(1);
			indexData.push_back(3);
			indexData.push_back(1);
			indexData.push_back(2);
			indexData.push_back(3);
			break;
		case EB::kShape_RenderQuad:

			size = size * 0.99f;
			//top
			vertex_aux.vertex = { -size, size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,1 };
			vertexData.push_back(vertex_aux);
			//bottom
			vertex_aux.vertex = { -size, -size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 0,0 };
			vertexData.push_back(vertex_aux);
			//left
			vertex_aux.vertex = { size, -size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 1,0 };
			vertexData.push_back(vertex_aux);
			//right
			vertex_aux.vertex = { size, size, 0.0f };
			vertex_aux.normal = { 0,0,1 };
			vertex_aux.texcoord = { 1,1 };
			vertexData.push_back(vertex_aux);


			indexData.push_back(0);
			indexData.push_back(1);
			indexData.push_back(3);
			indexData.push_back(1);
			indexData.push_back(2);
			indexData.push_back(3);
			break;
		
		default:
			break;
		}


		printf("");
	}

	///load obj
	void Geometry::load(std::string file)
	{
		std::string inputfile = file;
		
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		tinyobj::attrib_t attrib;
		std::string err;
		bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, inputfile.c_str());

		//if (!err.empty()) { // `err` may contain warning message.
		//	std::cerr << err << std::endl;
		//}

		if (!ret) {
			exit(1);
		}
				
		eb_uint16 num_index = 0;
		//get index, vertex, normals, texcoord
		for (size_t s = 0; s < shapes.size(); s++) {
			// Loop over faces(polygon)
			size_t index_offset = 0;
			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int fv = shapes[s].mesh.num_face_vertices[f];

				// Loop over vertices in the face.
				for (size_t v = 0; v < fv; v++) {
					
					// access to face vertex
					//index
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
					indexData.push_back(num_index);
					//vertex
					vertex_aux.vertex.x = attrib.vertices[3 * idx.vertex_index + 0] * size;
					vertex_aux.vertex.y = attrib.vertices[3 * idx.vertex_index + 1] * size;
					vertex_aux.vertex.z = attrib.vertices[3 * idx.vertex_index + 2] * size;
					//normal
					vertex_aux.normal.x = attrib.normals[3 * idx.normal_index + 0];
					vertex_aux.normal.y = attrib.normals[3 * idx.normal_index + 1];
					vertex_aux.normal.z = attrib.normals[3 * idx.normal_index + 2];
					//texcoord
					vertex_aux.texcoord.x = attrib.texcoords[2 * idx.texcoord_index + 0];
					vertex_aux.texcoord.y = attrib.texcoords[2 * idx.texcoord_index + 1];

					vertexData.push_back(vertex_aux);
					num_index++;
				}
				index_offset += fv;
			}
		}
	}
	void Geometry::compile()
	{
		
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);
		glBindVertexArray(VAO);
		
		//Vertex, normals, texcoord
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER,
			vertexData.size() * sizeof(VertexData),
			&vertexData.front(),
			GL_STATIC_DRAW);
				
		//Index
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			indexData.size() * sizeof(eb_uint32),
			&indexData.front(),
			GL_STATIC_DRAW);
		
		//position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(eb_float), 
			(void*)0);
		glEnableVertexAttribArray(0);
		
		//normal attributte
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(eb_float),
			(void*)(3 * sizeof(eb_float)));
		
		glEnableVertexAttribArray(1);
		
		//texture coord attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(eb_float),
			(void*)((6 * sizeof(eb_float))));
		
		glEnableVertexAttribArray(2);

		//wireframe mode
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	}

	void Geometry::draw()
	{
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, indexData.size(), GL_UNSIGNED_INT, 0);
			
	}

}
