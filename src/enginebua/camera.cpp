#include "camera.h"
#include "engine.h"
#include "glad\glad.h"
#include "GLFW\include\GLFW\glfw3.h"


namespace EB {
	
	Camera::Camera()
	{
		
	}
	Camera::~Camera()
	{

	}
	void Camera::Init(eb_float width, eb_float height)
	{
		EB::Engine *engine = EB::Engine::Instance();
		camPos = glm::vec3(0.0f, 20.0f, 20.0f);
		camTarget = glm::vec3(0.0f, 0.0f, 0.0f);
		camDirection = glm::vec3(0.0f, 0.0f, 1.0f);
		camUp = glm::vec3(0.0f, 1.0f, 0.0f);
		worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
		nearPlane = 5.0f;
		farPlane = 100.0f;
		view = GetViewMatrix();
		SetAspectRatio(width, height);
		camVelocity = 0.01f;
				
	}

	void Camera::SetAspectRatio(eb_float w, eb_float h)
	{
		aspectRatio = w / h;
	}

	void Camera::SetPosition(glm::vec3 pos)
	{
		camPos = pos;
	}

	void Camera::SetTarget(glm::vec3 tar)
	{
		camTarget = tar;
	}

	void Camera::SetNearFarPlane(eb_float near_plane, eb_float far_plane)
	{
		nearPlane = near_plane;
		farPlane = far_plane;
	}

	glm::mat4 Camera::GetViewMatrix()
	{
		/*
		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(pitch));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		camDirection = glm::normalize(front);
		// Also re-calculate the Right and Up vector
		camRight = glm::normalize(glm::cross(camDirection, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		camUp = glm::normalize(glm::cross(camRight, camDirection));
		*/
		
		
		camDirection = glm::normalize(camPos - camTarget);
		camRight = glm::normalize(glm::cross(glm::vec3{ 0.0,1.0,0.0 }, camDirection));
		glm::vec3 camUp = glm::cross(camDirection, camRight);
				
		return glm::lookAt(camPos, camDirection, camUp);
	}

	void Camera::MoveCamera() 
	{
		EB::Engine *engine = EB::Engine::Instance();

		int key = engine->window->CatchKeyboard();

		switch (key)
		{
		//W
		case 87:
			camPos -= camDirection * camVelocity;
			break;
		//S
		case 83:
			camPos += camDirection * camVelocity;
			break;
		//A
		case 65:
			camPos -= camRight * camVelocity;
			break;
		//D
		case 68:
			camPos += camRight * camVelocity;
			break;
		case 81:
			camPos.y -= camVelocity;
			break;
		case 69:
			camPos.y += camVelocity;
			break;
		default:
			break;
		}
	}

		
	void Camera::Update(int program,
		glm::mat4 model,
		unsigned int texture_id) {

		EB::Engine *engine = EB::Engine::Instance();

		MoveCamera();

		view = GetViewMatrix();
		projection = glm::perspective(glm::radians(45.0f), aspectRatio, nearPlane, farPlane);

		///Active Textures
		
		///Shadow1
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, engine->depht[0]);
		glUniform1i(glGetUniformLocation(program, "shadowMap0"),0);
		
		///Shadow2
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, engine->depht[1]);
		glUniform1i(glGetUniformLocation(program, "shadowMap1"), 1);

		///Shadow3
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, engine->depht[2]);
		glUniform1i(glGetUniformLocation(program, "shadowMap2"), 2);
		
		///Shadow4
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, engine->depht[3]);
		glUniform1i(glGetUniformLocation(program, "shadowMap3"), 3);

		///Color
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, texture_id);
		glUniform1i(glGetUniformLocation(program, "textureMap"), 4);

		///Set Light Space and Light Position 
		glm::mat4 lightspace[10];
		glm::vec3 lightpos[10];
		for (int i = 0; i < engine->lights.size(); i++) {
			
			lightpos[i] = engine->lights[i]->getWorldPosition();
			lightspace[i]= Engine::Instance()->lightMatrix[i];
		}

		//Set Numer of Lights
		int numLigths = engine->lights.size();

		//Set Uniforms Parameters
		glUniformMatrix4fv(glGetUniformLocation(program, "model") , 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(glGetUniformLocation(program, "view") , 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniform3fv(glGetUniformLocation(program, "lightPos"),10, (const GLfloat*)lightpos);
		glUniform3fv(glGetUniformLocation(program, "viewPos"), 1, glm::value_ptr(camPos));
		glUniformMatrix4fv(glGetUniformLocation(program, "lightSpaceMatrix"), 10, GL_FALSE,(const GLfloat*)lightspace);
		glUniform1i(glGetUniformLocation(program, "numLigths"), numLigths);
		glUniform1i(glGetUniformLocation(program, "postprocess"), engine->postprocess);
	}
}