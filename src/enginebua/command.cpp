#include "command.h"


namespace EB {

	Command::Command() {

	}
	Command::~Command() {

	}

	void Command::execute() {

		switch (comand_type)
		{
		case EB::kCommand_Empty:
			break;
		case EB::kCommand_Draw:
			
				node->execute(camera);

			break;
		case EB::kCommand_HighLight:
				node->executeHighLight(camera);

			break;
		
		default:
			break;
		}
	}

	void Command::executeShadows(Camera cam) {

		switch (comand_type)
		{
		case EB::kCommand_Empty:
			break;
		case EB::kCommand_Draw:

			node->execute(&cam);

			break;

		default:
			break;
		}
	}

	void Command::Create(commandType ct, Node * n, Camera *cam)
	{
		comand_type = ct;
		node = n;
		camera = cam;
	}

}