#include <stdio.h>
#include "chrono.h"
#include "GLFW/include/GLFW/glfw3.h"

namespace EB {

	Chrono::Chrono() {
		init_ = 0;
		end_ = 0;
		glfwSetTime(10);
		
	}
	Chrono::~Chrono(){}

	void Chrono::Start() {
		init_ = glfwGetTime();
	}

	double Chrono::Stop() {
		end_ = glfwGetTime() - init_;
		return end_;
	}
	void Chrono::ShowTime() {
		printf("Elapsed Time: %2.4f", end_*0.001f);
	}
}